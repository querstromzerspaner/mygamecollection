<?php
/**
 * Created by PhpStorm.
 * User: oxaya
 * Date: 3/10/2018
 * Time: 19:10
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class game extends Model
{
    protected $fillable = ['publisher', 'title', 'completed'];
}